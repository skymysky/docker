# Grafana

* [grafana/grafana GitHub repository](https://github.com/grafana/grafana)
* [Run Grafana Docker image](https://grafana.com/docs/grafana/latest/installation/docker/)
* [Deploy Grafana on Kubernetes](https://grafana.com/docs/grafana/latest/installation/kubernetes/)

